﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileMoverService
{
    public partial class Service1 : ServiceBase
    {
        // C:\Windows\Microsoft.NET\Framework\v4.0.30319

        // The Service is watching the Temp folder. 
        // The Service is also monitoring the C:\ADN folder and move any file created there to the destination folder you had created previously.
        // Runs in the background

        /// <summary>
        /// Initializes a new FileSysteWatcher, passing in the folder path of where the XML doc is from 11.2 Test Driven Development assign.
        /// FileSystemWatcher class can be used to monitor changes to file system and trigger events when such change occurs.
        /// </summary>
        private static FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(@"C:\ADN");

        // Folder path that I created anywhere on my computer/ destination path
        // private static string targetFolder = @"C:\ADNMovingFile";

        public Service1()
        {
           InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // Set the fileSystemWatcher EnableRaisingEvents to True which starts monitoring
            fileSystemWatcher.EnableRaisingEvents = true;

            // Attach OnCreatem, OnChange, and OnRename to the fileSystemWatcher's Created, Changed, and Renmed events.
            fileSystemWatcher.Created += OnCreate;
            fileSystemWatcher.Changed += OnChange;
            fileSystemWatcher.Renamed += OnRename;

            Console.ReadLine();

            // Instantiate an object of Event Log
            EventLog eventLog = new EventLog();

            // Set its source to "Application"
            eventLog.Source = "Application";

            // Then write entry to the log stating that service wass started.
            eventLog.WriteEntry("The Service was started", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            // Instantiate an object of Event Log
            EventLog eventLog = new EventLog();

            // Set its source to "Application"
            eventLog.Source = "Application";

            eventLog.WriteEntry("The service was stopped.", EventLogEntryType.Information);
        }

        /// <summary>
        /// The mover file, when running the service should monitor the C:\ADN folder and move any file created there to 
        /// the destination folder.
        /// </summary>
        /// <param name="sourcePath">The source path of the file.</param>
        /// <param name="destinationPath">The file destination.</param>
        private static void MoveFile(string sourcePath, string destinationPath)
        {
            if (File.Exists(sourcePath))
            {
                // Have the main thread sleep for one second to wait for any locks on the file to release.
                Thread.Sleep(1000);

                Monitor.Enter(sourcePath);
                try
                {
                    // Checks if the file exists at the destination path.
                    if (File.Exists(destinationPath))
                    {
                        // If file exists, then delete the source.
                        File.Delete(sourcePath);  
                    }

                    // Move the file from the sourcePath to the destinationPath.
                    File.Move(sourcePath, destinationPath);
                }
                finally
                {
                   Monitor.Exit(sourcePath);
                }

                Console.WriteLine("The file has moved the: " + sourcePath + "and" + destinationPath);
            }
            //else
            //{
               // Console.WriteLine("File path does not exists");
            //}
        }

        /// <summary>
        /// This event is triggered when a file or a directory in the path being monitored is changed.
        /// </summary>
        /// <param name="sender">The object that sends the file.</param>
        /// <param name="e">The file system.</param>
        private static void OnChange(object sender, FileSystemEventArgs e)
        {
            // Instantiate an object of Event Log
            EventLog eventLog2 = new EventLog();

            // Set its source to "Application"
            eventLog2.Source = "Application";

            // Then write entry to the log stating that the file could not be moved. Then give it an Entry Type Information.
            eventLog2.WriteEntry("File was moved", EventLogEntryType.Information);
        }

        /// <summary>
        /// This event is triggered when a file or a directory in the path being monitored is created.
        /// </summary>
        /// <param name="sender">The sender that of the created file.</param>
        /// <param name="e">The file system task.</param>
        private static void OnCreate(object sender, FileSystemEventArgs e)
        {
            try
            {
                // Use the FileSystemEventArgs objects FullPath and your target folder with the objects "Name" as the destination path.
                MoveFile(e.FullPath, Path.Combine(@"C:\ADNMovingFile", e.Name));
            }
            catch
            {
                // Instantiate an object of Event Log
                EventLog eventLog = new EventLog();

                // Set its source to "Application"
                eventLog.Source = "Application";

                // Then write entry to the log stating that the file could not be moved. Then give it an Entry Type Error.
                eventLog.WriteEntry("File could not be moved", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// This event is triggered when a file or a directory in the path being monitored is renamed.
        /// </summary>
        /// <param name="sender">The sender of the rename file.</param>
        /// <param name="e">The file system task.</param>
        private static void OnRename(object sender, FileSystemEventArgs e)
        {
            // Use MoveFile the same way you did in the OnCreate.
            try
            {
                // Use the FileSystemEventArgs ohjects FullPath and your target folder with the objects "Name" as the destination path.
                MoveFile(e.FullPath, Path.Combine(@"C:\ADNMovingFile", e.Name));
            }
            catch
            {
                // Instantiate an object of Event Log
                EventLog eventLog = new EventLog();

                // Set its source to "Application"
                eventLog.Source = "Application";

                // Then write entry to the log stating that the file could not be moved. Then give it an Entry Type Error.
                eventLog.WriteEntry("File could not be moved", EventLogEntryType.Error);
            }
        }
    }
}

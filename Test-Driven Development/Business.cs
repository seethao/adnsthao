﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent the business.
    /// </summary>
    [Serializable]
    public class Business
    {
        /// <summary>
        /// Initialiazes the new business method.
        /// </summary>
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        /// <summary>
        /// Gets or Sets the employess.
        /// </summary>
        public List<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or Sets the jobs.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Add the employee to the list of employess.
        /// </summary>
        /// <param name="employee">The name of the employee.</param>
        public void AddEmployee(Employee employee)
        {
            // Add employee to the employee list.
            this.Employees.Add(employee);
        }

        /// <summary>
        /// Add the job to the job list.
        /// </summary>
        /// <param name="job">The position of the job.</param>
        public void AddJob(Job job)
        {
            // Add job to the job list
            this.Jobs.Add(job);
        }

        /// <summary>
        /// Do the work.
        /// </summary>
        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork().
            // If job is completed after work, break from loop.
            foreach (Job j in this.Jobs)
            {
                if (!j.JobCompleted)
                {
                    foreach (Employee e in this.Employees)
                    {
                        e.DoWork(j);

                        // If the job completed then break out of the loop.
                        if (j.JobCompleted)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent an employee.
    /// </summary>
    [Serializable]
    public class Employee
    {
        /// <summary>
        /// The hourly wage of the employee.
        /// </summary>
        private double hourlyWage;

        /// <summary>
        /// The schedule hours for the employee.
        /// </summary>
        private double hoursScheduled;

        /// <summary>
        /// Initializes a new Employee.
        /// </summary>
        /// <param name="hourlyWage">The amount of the hourly wage.</param>
        /// <param name="hoursScheduled">The hours that is available to the employee.</param>
        public Employee(double hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        /// <summary>
        /// Initializes a new Employee.
        /// </summary>
        private Employee()
        {
        }

        /// <summary>
        /// Gets or Sets the Paycheck.
        /// </summary>
        public double Paycheck { get; set; }

        /// <summary>
        /// Gets or Sets the Hours Paid.
        /// </summary>
        public double HoursPaid { get; set; }

        /// <summary>
        /// Gets or Sets the employee Service.
        /// </summary>
        public double Service { get; set; }

        /// <summary>
        /// Do the work.
        /// </summary>
        /// <param name="work">The job that is being worked on.</param>
        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, reduce the hours remaining on the job by the hours scheduled,
            // and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.
            if (this.hoursScheduled <= work.TimeInvestmentRemaining)
            {
                // hour schedule minus the time investment
                work.TimeInvestmentRemaining -= this.hoursScheduled;

                // Multiply the hours worked by the hourly wage and add this to their paycheck property.(testing)
                this.HoursPaid = this.hoursScheduled * this.hourlyWage;

                // Add it to the Paycheck property.
                this.HoursPaid += this.Paycheck;

                // Set the JobCost equal to 1.5x the cost of paying the employee for the service.
                this.Service = 1.5 * this.hoursScheduled;

                // Set the service to the job cost.
                work.JobCost += this.Service;

                // Set the employee hours to 0.
                this.hoursScheduled = 0;
            }
            else
            {
                this.hoursScheduled -= work.TimeInvestmentRemaining;

                // Set the time investment to 0.
                work.TimeInvestmentRemaining = 0;
            }
        }
    }
}

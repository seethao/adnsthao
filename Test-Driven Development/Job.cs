﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent a job.
    /// </summary>
    [Serializable]
    public class Job
    {
        /// <summary>
        /// Initializes a new job method.
        /// </summary>
        /// <param name="timeInvestment">The time investment of the job.</param>
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        /// <summary>
        /// Initializes a new job.
        /// </summary>
        private Job()
        {
        }

        /// <summary>
        /// Gets or Sets the Time investment remaining is how many hours is left to finish the job.
        /// </summary>
        public double TimeInvestmentRemaining { get; set; }

        /// <summary>
        /// Gets a value indicating whether true or false if job is completed or not.
        /// </summary>
        public bool JobCompleted => this.TimeInvestmentRemaining == 0;

        /// <summary>
        /// Gets or sets the JobCost.
        /// </summary>
        public double JobCost { get; set; }
    }
}

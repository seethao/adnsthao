﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class which is used to represent the program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main method of the program.
        /// </summary>
        /// <param name="args">The event arg of the method.</param>
        public static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 15));
            business.AddEmployee(new Employee(10, 7));
            business.AddJob(new Job(15));
            business.AddJob(new Job(15));
            business.DoWork();

            SerializeBusiness(business);

            Console.ReadLine();
        }

        /// <summary>
        /// The serialize business method (saving application state).
        /// </summary>
        /// <param name="business">The business.</param>
        public static void SerializeBusiness(Business business)
        {
            // Create a binary formatter XML formatter creates an instance of the object in order to save the XML file,
            // It requires parameterless constructor to be available. Serializable to Job and Employee.
            BinaryFormatter formatter = new BinaryFormatter();

            // Create file using the passed-in file name
            // Use a using statement to automatically clean up object references
            // and close the file handle when the serialization process is complete
            using (Stream stream = File.Create(@"C:\ADN\TestDrivenDevelopment.xml"))
            {
                // Serialize (save) the current instance of the business.
                formatter.Serialize(stream, business);
            }
        }
    }
}
